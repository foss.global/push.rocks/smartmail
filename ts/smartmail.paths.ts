import * as plugins from './smartmail.plugins.js';

export const packageDir = plugins.path.join(
  plugins.smartpath.get.dirnameFromImportMetaUrl(import.meta.url),
  '../'
);
export const assetDir = plugins.path.join(packageDir, './assets');
